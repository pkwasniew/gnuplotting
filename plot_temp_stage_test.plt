set datafile separator ";"
set key autotitle columnhead
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 4 \
    pointtype 7 pointsize 1.5

set xlabel "Time [s]"
set ylabel "Temperature [deg C]"
set title "Temperature stage test"

#set logscale y 10
set grid

set key noenhanced

colors = "red #0000FF violet orange"
set for [i=1:words(colors)] linetype i lc rgb word(colors, i)
set y2tics 0.1,0.1
#set format y2 "10^{%L}"
set ytics nomirror
set y2label "Pressure [mbar]"
#set logscale y2
plot for [data in filename] data using 1:2 with lines title data axis x1y1, for [data in filename] data using 1:3 with lines axis x1y2

#set term png size 1024,648
#set output outfile
#replot

pause 2 
reread
set autoscale

#pause -1 "Hit any key to continue"

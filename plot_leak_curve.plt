set datafile separator ";"
set key autotitle columnhead
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 2 \
    pointtype 7 pointsize 1.5

set xlabel "Time [s]"
set ylabel "Pressure [mBar]"
#set title "Leakage curve"

set grid

set key noenhanced

f(x) = a*x + b

fit f(x) filename using 1:2 via a,b

#v = 417.8
leak_rate = a * volume
set title sprintf("Leakage rate: %.2e mBar L/s; volume: %.2fL", leak_rate, volume)
#set label sprintf("Leakage rate: %.2e mBar L/s", leak_rate) at 600,0.12
plot filename with lines linestyle 1 title filename, f(x) with lines title "Linear fit"

set term png size 1024,648
set output outfile
replot

pause -1 "Hit any key to continue"

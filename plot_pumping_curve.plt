set datafile separator ";"
set key autotitle columnhead
set style line 1 \
    linecolor rgb '#0060ad' \
    linetype 1 linewidth 4 \
    pointtype 7 pointsize 1.5

set xlabel "Time [s]"
set ylabel "Pressure [mBar]"
set title "Pumping curve"

set logscale y 10
set grid

set key noenhanced

colors = "red #0000FF violet orange"
set for [i=1:words(colors)] linetype i lc rgb word(colors, i)

plot for [data in filename] data with lines title data

#set term png size 1024,648
#set output outfile
#replot

#pause -1 "Hit any key to continue"

# Live plot
pause 2
reread
autoscale

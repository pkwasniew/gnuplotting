#!/bin/bash

if [ -z "$1" ]
  then
    echo "Usage: $0 /path/to/data_file.txt"
    exit
fi

filename=$1
outfile="${filename%.txt}.png"

if [ ! -f $filename ]; then
    echo "File $filename not found!"
    exit
fi

leak_plot="plot_leak_curve.plt"
pumping_plot="plot_pumping_curve.plt"

if echo "$filename" | grep -q "leak"; then
    if [ -z "$2" ]
      then
        echo "Usage: $0 /path/to/data_file.txt <system volume [L]>"
        exit
    fi
    gnuplot -e "filename='$filename'; outfile='$outfile'; volume=$2" $leak_plot
elif echo "$filename" | grep -q "pumping"; then
    filename="'$*'"
    gnuplot -e "filename=$filename; outfile='$outfile'" $pumping_plot
else
    echo "I don't know what to do with $filename"
    echo "Please provide a valid vacuum test output data file"
    exit
fi

#!/bin/bash

if [ -z "$1" ]
  then
    echo "Usage: $0 /path/to/data_file.txt"
    exit
fi

filename=$1
outfile="${filename%.txt}.png"

if [ ! -f $filename ]; then
    echo "File $filename not found!"
    exit
fi

test_plot="plot_temp_stage_test.plt"

filename="'$*'"
gnuplot -e "filename=$filename; outfile='$outfile'" $test_plot
